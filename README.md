# Demo
### 使用方法
运行 python server.py，然后前往xx.xxx.xxx.xxx:port。port为server.py中监听的端口，xx.xxx.xxx.xxx为机器的ip。
### 关于更换模型  
如果要更换新的模型：  
1. 把模型的定义拷贝到model.py；
2. 把模型参数保存的文件路径(通常是.pth)，写到config.py的model_path字典中键为"SpeechEmotion_path"的值中。
3. 可配合修改predict.py文件中的Predict类的init_model_state方法中这一行：
    ~~~python
    self.SpeechEmotion_model.load_state_dict(torch.load(model_path["SpeechEmotion_path"]))
    ~~~
4. 如果有必要，修改predict.py文件中的Predict类的get_emotion_scores方法。该方法执行了推断的过程。  

## 目前效果  
支持显示语谱图、波形图、播放音频、展示各类分数。  
![img.png](img.png)
