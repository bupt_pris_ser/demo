esc50_labels = ["dog", "rooster", "pig", "cow", "frog", "cat", "hen", "insects", "sheep", "crow", "rain",
                "sea_waves", "crackling_fire", "crickets", "chirping_birds", "water_drops", "wind", "pouring_water",
                "toilet_flush", "thunderstorm", "crying_baby", "sneezing", "clapping", "breathing", "coughing",
                "footsteps", "laughing", "brushing_teeth", "snoring", "drinking_sipping", "door_wood_knock",
                "mouse_click", "keyboard_typing", "door_wood_creaks", "can_opening", "washing_machine",
                "vacuum_cleaner", "clock_alarm", "clock_tick", "glass_breaking", "helicopter", "chainsaw",
                "siren", "car_horn", "engine", "train", "church_bells", "airplane", "fireworks", "hand_saw"]

urbansound_labels = ["air_conditioner", "car_horn", "children_playing", "dog_bark", "drilling", "engine_idling",
                     "gun_shot", "jackhammer", "siren", "street_music"]

FSDKaggle_labels = ['Accelerating_and_revving_and_vroom', 'Accordion', 'Acoustic_guitar', 'Applause', 'Bark',
                    'Bass_drum',
                    'Bass_guitar', 'Bathtub_(filling_or_washing)', 'Bicycle_bell', 'Burping_and_eructation', 'Bus',
                    'Buzz',
                    'Car_passing_by', 'Cheering', 'Chewing_and_mastication', 'Child_speech_and_kid_speaking',
                    'Chink_and_clink', 'Chirp_and_tweet', 'Church_bell', 'Clapping', 'Computer_keyboard',
                    'Crackle', 'Cricket', 'Crowd',
                    'Cupboard_open_or_close', 'Cutlery_and_silverware', 'Dishes_and_pots_and_pans',
                    'Drawer_open_or_close',
                    'Drip', 'Electric_guitar', 'Fart', 'Female_singing', 'Female_speech_and_woman_speaking',
                    'Fill_(with_liquid)',
                    'Finger_snapping', 'Frying_(food)', 'Gasp', 'Glockenspiel', 'Gong', 'Gurgling', 'Harmonica',
                    'Hi-hat',
                    'Hiss', 'Keys_jangling', 'Knock', 'Male_singing', 'Male_speech_and_man_speaking',
                    'Marimba_and_xylophone',
                    'Mechanical_fan', 'Meow', 'Microwave_oven', 'Motorcycle', 'Printer', 'Purr',
                    'Race_car_and_auto_racing',
                    'Raindrop', 'Run', 'Scissors', 'Screaming', 'Shatter', 'Sigh', 'Sink_(filling_or_washing)',
                    'Skateboard', 'Slam',
                    'Sneeze', 'Squeak', 'Stream', 'Strum', 'Tap', 'Tick-tock', 'Toilet_flush',
                    'Traffic_noise_and_roadway_noise', 'Trickle_and_dribble', 'Walk_and_footsteps',
                    'Water_tap_and_faucet',
                    'Waves_and_surf', 'Whispering', 'Writing', 'Yell', 'Zipper_(clothing)']

SpeechEmotion_labels = ['ang', " hap", 'neu', 'sad']

model_path = {
    "urban_path_zhuceng1": "/home/hongxiaofeng/urbanSound8K/train/"
                           "urbansound_project/save_model/urban"
                           "/56zhuceng_train1.pkl",
    "urban_path_zhuceng2": "/home/hongxiaofeng/urbanSound8K/train/"
                           "urbansound_project/save_model/urban"
                           "/56zhuceng_train2.pkl",
    "urban_path_zhuceng3": "/home/hongxiaofeng/urbanSound8K/train/"
                           "urbansound_project/save_model/urban"
                           "/56zhuceng_train3.pkl",
    "urban_path_zhuceng4": "/home/hongxiaofeng/urbanSound8K/train/"
                           "urbansound_project/save_model/urban"
                           "/56zhuceng_train4.pkl",
    "urban_path_zhuceng5": "/home/hongxiaofeng/urbanSound8K/train/"
                           "urbansound_project/save_model/urban"
                           "/56zhuceng_train5.pkl",
    "urban_path_zhuceng6": "/home/hongxiaofeng/urbanSound8K/train/"
                           "urbansound_project/save_model/urban"
                           "/56zhuceng_train6.pkl",
    "urban_path_zhuceng7": "/home/hongxiaofeng/urbanSound8K/train/"
                           "urbansound_project/save_model/urban"
                           "/56zhuceng_train7.pkl",
    "urban_path_zhuceng8": "/home/hongxiaofeng/urbanSound8K/train/"
                           "urbansound_project/save_model/urban"
                           "/56zhuceng_train8.pkl",
    "urban_path_zhuceng9": "/home/hongxiaofeng/urbanSound8K/train/"
                           "urbansound_project/save_model/urban"
                           "/56zhuceng_train9.pkl",
    "urban_path_zhuceng10": "/home/hongxiaofeng/urbanSound8K/train/"
                            "urbansound_project/save_model/urban"
                            "/56zhuceng_train10.pkl",
    "urban_path_zhouqi1": "/home/hongxiaofeng/urbanSound8K/train/"
                          "urbansound_project/save_model/urban"
                          "/urban_56zhouqi_train1.pkl",
    "urban_path_zhouqi2": "/home/hongxiaofeng/urbanSound8K/train/"
                          "urbansound_project/save_model/urban"
                          "/urban_56zhouqi_train2.pkl",
    "urban_path_zhouqi3": "/home/hongxiaofeng/urbanSound8K/train/"
                          "urbansound_project/save_model/urban"
                          "/urban_56zhouqi_train3.pkl",
    "urban_path_zhouqi4": "/home/hongxiaofeng/urbanSound8K/train/"
                          "urbansound_project/save_model/urban"
                          "/urban_56zhouqi_train4.pkl",
    "urban_path_zhouqi5": "/home/hongxiaofeng/urbanSound8K/train/"
                          "urbansound_project/save_model/urban"
                          "/urban_56zhouqi_train5.pkl",
    "urban_path_zhouqi6": "/home/hongxiaofeng/urbanSound8K/train/"
                          "urbansound_project/save_model/urban"
                          "/urban_56zhouqi_train6.pkl",
    "urban_path_zhouqi7": "/home/hongxiaofeng/urbanSound8K/train/"
                          "urbansound_project/save_model/urban"
                          "/urban_56zhouqi_train7.pkl",
    "urban_path_zhouqi8": "/home/hongxiaofeng/urbanSound8K/train/"
                          "urbansound_project/save_model/urban"
                          "/urban_56zhouqi_train8.pkl",
    "urban_path_zhouqi9": "/home/hongxiaofeng/urbanSound8K/train/"
                          "urbansound_project/save_model/urban"
                          "/urban_56zhouqi_train9.pkl",
    "urban_path_zhouqi10": "/home/hongxiaofeng/urbanSound8K/train/"
                           "urbansound_project/save_model/urban"
                           "/urban_56zhouqi_train10.pkl",
    "urban_path_chixu1": "/home/hongxiaofeng/urbanSound8K/train/"
                         "urbansound_project/save_model/urban"
                         "/duosunshi1024shuxing_chixu_crossEn_train1.pkl",
    "urban_path_chixu2": "/home/hongxiaofeng/urbanSound8K/train/"
                         "urbansound_project/save_model/urban"
                         "/duosunshi1024shuxing_chixu_crossEn_train2.pkl",
    "urban_path_chixu3": "/home/hongxiaofeng/urbanSound8K/train/"
                         "urbansound_project/save_model/urban"
                         "/duosunshi1024shuxing_chixu_crossEn_train3.pkl",
    "urban_path_chixu4": "/home/hongxiaofeng/urbanSound8K/train/"
                         "urbansound_project/save_model/urban"
                         "/duosunshi1024shuxing_chixu_crossEn_train4.pkl",
    "urban_path_chixu5": "/home/hongxiaofeng/urbanSound8K/train/"
                         "urbansound_project/save_model/urban"
                         "/duosunshi1024shuxing_chixu_crossEn_train5.pkl",
    "urban_path_chixu6": "/home/hongxiaofeng/urbanSound8K/train/"
                         "urbansound_project/save_model/urban"
                         "/duosunshi1024shuxing_chixu_crossEn_train6.pkl",
    "urban_path_chixu7": "/home/hongxiaofeng/urbanSound8K/train/"
                         "urbansound_project/save_model/urban"
                         "/duosunshi1024shuxing_chixu_crossEn_train7.pkl",
    "urban_path_chixu8": "/home/hongxiaofeng/urbanSound8K/train/"
                         "urbansound_project/save_model/urban"
                         "/duosunshi1024shuxing_chixu_crossEn_train8.pkl",
    "urban_path_chixu9": "/home/hongxiaofeng/urbanSound8K/train/"
                         "urbansound_project/save_model/urban"
                         "/duosunshi1024shuxing_chixu_crossEn_train9.pkl",
    "urban_path_chixu10": "/home/hongxiaofeng/urbanSound8K/train/"
                          "urbansound_project/save_model/urban"
                          "/duosunshi1024shuxing_chixu_crossEn_train10.pkl",
    "urban_path_xiezhen1": "/home/hongxiaofeng/urbanSound8K/train/"
                           "urbansound_project/save_model/urban"
                           "/duosunshi1024shuxing_xiezhen_crossEn_train1.pkl",
    "urban_path_xiezhen2": "/home/hongxiaofeng/urbanSound8K/train/"
                           "urbansound_project/save_model/urban"
                           "/duosunshi1024shuxing_xiezhen_crossEn_train2.pkl",
    "urban_path_xiezhen3": "/home/hongxiaofeng/urbanSound8K/train/"
                           "urbansound_project/save_model/urban"
                           "/duosunshi1024shuxing_xiezhen_crossEn_train3.pkl",
    "urban_path_xiezhen4": "/home/hongxiaofeng/urbanSound8K/train/"
                           "urbansound_project/save_model/urban"
                           "/duosunshi1024shuxing_xiezhen_crossEn_train4.pkl",
    "urban_path_xiezhen5": "/home/hongxiaofeng/urbanSound8K/train/"
                           "urbansound_project/save_model/urban"
                           "/duosunshi1024shuxing_xiezhen_crossEn_train5.pkl",
    "urban_path_xiezhen6": "/home/hongxiaofeng/urbanSound8K/train/"
                           "urbansound_project/save_model/urban"
                           "/duosunshi1024shuxing_xiezhen_crossEn_train6.pkl",
    "urban_path_xiezhen7": "/home/hongxiaofeng/urbanSound8K/train/"
                           "urbansound_project/save_model/urban"
                           "/duosunshi1024shuxing_xiezhen_crossEn_train7.pkl",
    "urban_path_xiezhen8": "/home/hongxiaofeng/urbanSound8K/train/"
                           "urbansound_project/save_model/urban"
                           "/duosunshi1024shuxing_xiezhen_crossEn_train8.pkl",
    "urban_path_xiezhen9": "/home/hongxiaofeng/urbanSound8K/train/"
                           "urbansound_project/save_model/urban"
                           "/duosunshi1024shuxing_xiezhen_crossEn_train9.pkl",
    "urban_path_xiezhen10": "/home/hongxiaofeng/urbanSound8K/train/"
                            "urbansound_project/save_model/urban"
                            "/duosunshi1024shuxing_xiezhen_crossEn_train10.pkl",
    "esc50_path_zhuceng": "/home/hongxiaofeng/urbanSound8K/train/"
                          "urbansound_project/save_model"
                          "/esc_56zhuceng_esc_train.pkl",
    "esc50_path_zhouqi": "/home/hongxiaofeng/urbanSound8K/train/"
                         "urbansound_project/save_model"
                         "/esc_56zhouqi_esc_train.pkl",
    "esc50_path_chixu": "/home/hongxiaofeng/urbanSound8K/train/"
                        "urbansound_project/save_model"
                        "/esc_56chixu_esc_train.pkl",
    "esc50_path_xiezhen": "/home/hongxiaofeng/urbanSound8K/train/"
                          "urbansound_project/save_model"
                          "/esc_56xiezhen_esc_train.pkl",
    "FSDKaggle_path": "/home/hongxiaofeng/aa/Freesound-Audio-Tagging-2019-master/models"
                      "/vgg_pse_disange.pth",
    "SpeechEmotion_path": "/home/wangce/third_year/demo/models_to_load/emotion.pth"
}
