# coding: utf-8
import tornado.ioloop
import tornado.web
import shutil
import os
import json
from predict import predict_object
from Creating_feature import *


class ESC50Handler(tornado.web.RequestHandler):
    def get(self):
        self.render("base.html")

    def post(self):

        upload_path = os.path.join(os.path.dirname(__file__), "wave_files")  # 文件的暂存路径
        file_metas = self.request.files.get('file', None)  # 提取表单中‘name'为‘file'的文件元数据

        if not file_metas:
            ret['result'] = 'Invalid Args'
            return ret

        for meta in file_metas:
            filename = meta['filename']
            file_path = os.path.join(upload_path, filename)
            with open(file_path, 'wb') as up:
                up.write(meta['body'])
                # OR do other thing
            input = convert_wav_to_image(file_path, "esc50")
            output_zhuceng, output_zhouqi, output_chixu, output_xiezhen = predict_object.predict_esc_labels(input)
            ret = {'esc_model_zhuceng': output_zhuceng,
                   'esc_model_zhouqi': output_zhouqi,
                   'esc_model_chixu': output_chixu,
                   'esc_model_xiezhen': output_xiezhen}

        self.write(json.dumps(ret))