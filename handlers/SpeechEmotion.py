# coding: utf-8
import tornado.ioloop
import tornado.web
import shutil
import os
import json
from predict import predict_object
from Creating_feature import *


class SpeechEmotionHandler(tornado.web.RequestHandler):
    def get(self):
        self.render("./predict.html", ang=0, hap=0, neu=0, sad=0, img=None, wav=None)

    def post(self):

        upload_path = os.path.join(os.path.dirname(__file__), "wave_files")  # 文件的暂存路径
        file_metas = self.request.files.get('file', None)  # 提取表单中‘name'为‘file'的文件元数据

        if not file_metas:
            return
        for meta in file_metas:
            filename = meta['filename']
            file_path = os.path.join(upload_path, filename)
            with open(file_path, 'wb') as up:
                up.write(meta['body'])
                # OR do other thing
            inp = convert_SpeechEmotion(file_path)
            img_base64, wav_base64 = predict_object.prepare_plot_and_wav(file_path)
            scores = predict_object.get_emotion_scores(inp)
        self.render("./predict.html", ang=scores[0], hap=scores[1], neu=scores[2], sad=scores[3], img=img_base64, wav=wav_base64)
