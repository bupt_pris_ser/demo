import torch
import torch.nn as nn
torch.backends.cudnn.deterministic = True
torch.backends.cudnn.benchmark = False
import math
import torch.nn.functional as F


def init_layer(layer):
    if layer.weight.ndimension() == 4:
        (n_out, n_in, height, width) = layer.weight.size()
        n = n_in * height * width

    elif layer.weight.ndimension() == 2:
        (n_out, n) = layer.weight.size()

    std = math.sqrt(2. / n)
    scale = std * math.sqrt(3.)
    layer.weight.data.uniform_(-scale, scale)

    if layer.bias is not None:
        layer.bias.data.fill_(0.)


class ConvBlock(nn.Module):
    def __init__(self, in_channels, out_channels):

        super(ConvBlock, self).__init__()

        self.conv1 = nn.Conv2d(in_channels=in_channels,
                               out_channels=out_channels,
                               kernel_size=(3, 3), stride=(1, 1),
                               padding=(1, 1), bias=False)

        self.conv2 = nn.Conv2d(in_channels=out_channels,
                               out_channels=out_channels,
                               kernel_size=(3, 3), stride=(1, 1),
                               padding=(1, 1), bias=False)

        self.bn1 = nn.BatchNorm2d(out_channels)
        self.bn2 = nn.BatchNorm2d(out_channels)

        self.init_weight()

    def init_weight(self):
        init_layer(self.conv1)
        init_layer(self.conv2)
        init_bn(self.bn1)
        init_bn(self.bn2)

    def forward(self, input, pool_size=(2, 2), pool_type='max'):

        x = input
        x = F.relu_(self.bn1(self.conv1(x)))
        x = F.relu_(self.bn2(self.conv2(x)))
        if pool_type == 'max':
            x = F.max_pool2d(x, kernel_size=pool_size)
        elif pool_type == 'avg':
            x = F.avg_pool2d(x, kernel_size=pool_size)
        elif pool_type == 'avg+max':
            x1 = F.avg_pool2d(x, kernel_size=pool_size)
            x2 = F.max_pool2d(x, kernel_size=pool_size)
            x = x1 + x2
        else:
            raise Exception('Incorrect argument!')

        return x


def init_bn(bn):
    """Initialize a Batchnorm layer. """

    bn.bias.data.fill_(0.)
    bn.weight.data.fill_(1.)


class VggishConvBlock(nn.Module):
    def __init__(self, in_channels, out_channels):
        super(VggishConvBlock, self).__init__()

        self.conv1 = nn.Conv2d(in_channels=in_channels,
                               out_channels=out_channels,
                               kernel_size=(3, 3), stride=(1, 1),
                               padding=(1, 1), bias=False)

        self.conv2 = nn.Conv2d(in_channels=out_channels,
                               out_channels=out_channels,
                               kernel_size=(3, 3), stride=(1, 1),
                               padding=(1, 1), bias=False)

        self.bn1 = nn.BatchNorm2d(out_channels)

        self.bn2 = nn.BatchNorm2d(out_channels)

        self.init_weights()

    def init_weights(self):
        init_layer(self.conv1)
        init_layer(self.conv2)
        init_bn(self.bn1)
        init_bn(self.bn2)

    def forward(self, input):
        x = input
        x = F.relu(self.bn1(self.conv1(x)))
        x = F.relu(self.bn2(self.conv2(x)))
        x = F.max_pool2d(x, kernel_size=(2, 2), stride=(2, 2))

        return x


def init_layer_1d(layer):
    """Initialize a Linear or Convolutional layer. """
    nn.init.xavier_uniform_(layer.weight)

    if hasattr(layer, 'bias'):
        if layer.bias is not None:
            layer.bias.data.fill_(0.)


class ConvPreWavBlock(nn.Module):
    def __init__(self, in_channels, out_channels):
        super(ConvPreWavBlock, self).__init__()

        self.conv1 = nn.Conv1d(in_channels=in_channels,
                               out_channels=out_channels,
                               kernel_size=3, stride=1,
                               padding=1, bias=False)

        self.conv2 = nn.Conv1d(in_channels=out_channels,
                               out_channels=out_channels,
                               kernel_size=3, stride=1, dilation=2,
                               padding=2, bias=False)

        self.bn1 = nn.BatchNorm1d(out_channels)
        self.bn2 = nn.BatchNorm1d(out_channels)

        self.init_weight()

    def init_weight(self):
        init_layer_1d(self.conv1)
        init_layer_1d(self.conv2)
        init_bn(self.bn1)
        init_bn(self.bn2)

    def forward(self, input, pool_size):
        x = input
        x = F.relu_(self.bn1(self.conv1(x)))
        x = F.relu_(self.bn2(self.conv2(x)))
        x = F.max_pool1d(x, kernel_size=pool_size)

        return x


class Vggish_esc(nn.Module):
    def __init__(self, in_channels, classes_num):
        super(Vggish_esc, self).__init__()

        self.conv_block1 = VggishConvBlock(in_channels=in_channels, out_channels=64)
        self.conv_block2 = VggishConvBlock(in_channels=64, out_channels=128)
        self.conv_block3 = VggishConvBlock(in_channels=128, out_channels=256)
        self.conv_block4 = VggishConvBlock(in_channels=256, out_channels=512)
        self.conv_block5 = VggishConvBlock(in_channels=512, out_channels=1024)

        self.fc_ = nn.Linear(512 * 2, 512, bias=True)
        self.fc_final = nn.Linear(1024, classes_num, bias=True)
        self.fc_final_1 = nn.Linear(64, classes_num, bias=True)
        self.fc_final_2 = nn.Linear(128, classes_num, bias=True)
        self.fc_final_3 = nn.Linear(256, classes_num, bias=True)
        self.fc_final_6 = nn.Linear(512, classes_num, bias=True)

        self.fc_final_4 = nn.Linear(250, 512, bias=True)
        self.fc_final_5 = nn.Linear(512, classes_num, bias=True)

        self.fc_shuxing_1 = nn.Linear(1024, 512, bias=True)
        self.fc_shuxing_2 = nn.Linear(512, 2, bias=True)

        self.conv_drop = nn.Dropout2d(0.3)
        self.drop = nn.Dropout(0.4)
        self.init_weights()

    def init_weights(self):
        init_layer(self.fc_final)
        init_layer(self.fc_final_1)
        init_layer(self.fc_final_2)
        init_layer(self.fc_final_3)
        init_layer(self.fc_final_4)
        init_layer(self.fc_final_5)
        init_layer(self.fc_final_6)

    def forward(self, input, input_v=None, valid=False):
        x = input
        # x2 = input_v
        '''(samples_num, feature_maps, time_steps, freq_num)'''

        x = self.conv_block1(x)  # 16, 64, 64, 100
        x1 = x
        (bottleneck1, _) = torch.max(x1, dim=-1)
        x1 = torch.mean(bottleneck1, dim=-1)

        x = self.conv_block2(x)
        x2 = x
        (bottleneck2, _) = torch.max(x2, dim=-1)
        x2 = torch.mean(bottleneck2, dim=-1)

        x = self.conv_block3(x)
        x3 = x
        (bottleneck3, _) = torch.max(x3, dim=-1)
        x3 = torch.mean(bottleneck3, dim=-1)

        x = self.conv_block4(x)
        x4 = x
        (bottleneck4, _) = torch.max(x4, dim=-1)
        x4 = torch.mean(bottleneck4, dim=-1)

        x = self.conv_block5(x)
        (bottleneck, _) = torch.max(x, dim=-1)
        '''(samples_num, feature_maps, time_steps)'''

        # Averaging out time axis
        x = torch.mean(bottleneck, dim=-1)

        '''
        x2 = self.conv_block1(x2)
        x2 = self.conv_block2(x2)
        x2 = self.conv_block3(x2)
        x2 = self.conv_block4(x2)
        (bottleneck2, _) = torch.max(x2, dim=-1)

        # Averaging out time axis
        x2 = torch.mean(bottleneck2, dim=-1)
        #print(x.shape, x2.shape)
        x = torch.cat((x, x2), 1)
        '''
        y = self.fc_final(x)
        y1 = self.fc_final_1(x1)
        y2 = self.fc_final_2(x2)
        y3 = self.fc_final_3(x3)
        y4 = self.fc_final_6(x4)

        out = torch.cat((y, y1, y2, y3, y4), dim=1)
        # out = F.relu_(self.fc_final_4(out))
        out = F.relu_(self.fc_final_4(out))
        out = self.fc_final_5(out)

        return out, y, y1, y2, y3, y4

class Vggish_urbansound(nn.Module):
    def __init__(self, in_channels, classes_num):
        super(Vggish_urbansound, self).__init__()

        self.conv_block1 = VggishConvBlock(in_channels=in_channels, out_channels=64)
        self.conv_block2 = VggishConvBlock(in_channels=64, out_channels=128)
        self.conv_block3 = VggishConvBlock(in_channels=128, out_channels=256)
        self.conv_block4 = VggishConvBlock(in_channels=256, out_channels=512)
        self.conv_block5 = VggishConvBlock(in_channels=512, out_channels=1024)

        self.fc_ = nn.Linear(512 * 2, 512, bias=True)
        self.fc_final = nn.Linear(1024, classes_num, bias=True)
        self.fc_final_1 = nn.Linear(64, classes_num, bias=True)
        self.fc_final_2 = nn.Linear(128, classes_num, bias=True)
        self.fc_final_3 = nn.Linear(256, classes_num, bias=True)
        self.fc_final_6 = nn.Linear(512, classes_num, bias=True)

        self.fc_final_4 = nn.Linear(50, 512, bias=True)
        self.fc_final_5 = nn.Linear(512, classes_num, bias=True)

        self.fc_shuxing_1 = nn.Linear(1024, 512, bias=True)
        self.fc_shuxing_2 = nn.Linear(512, 2, bias=True)

        self.conv_drop = nn.Dropout2d(0.3)
        self.drop = nn.Dropout(0.4)
        self.init_weights()

    def init_weights(self):
        init_layer(self.fc_final)
        init_layer(self.fc_final_1)
        init_layer(self.fc_final_2)
        init_layer(self.fc_final_3)
        init_layer(self.fc_final_4)
        init_layer(self.fc_final_5)
        init_layer(self.fc_final_6)

    def forward(self, input, input_v=None, valid=False):
        x = input
        # x2 = input_v
        '''(samples_num, feature_maps, time_steps, freq_num)'''

        x = self.conv_block1(x)  # 16, 64, 64, 100
        x1 = x
        (bottleneck1, _) = torch.max(x1, dim=-1)
        x1 = torch.mean(bottleneck1, dim=-1)

        x = self.conv_block2(x)
        x2 = x
        (bottleneck2, _) = torch.max(x2, dim=-1)
        x2 = torch.mean(bottleneck2, dim=-1)

        x = self.conv_block3(x)
        x3 = x
        (bottleneck3, _) = torch.max(x3, dim=-1)
        x3 = torch.mean(bottleneck3, dim=-1)

        x = self.conv_block4(x)
        x4 = x
        (bottleneck4, _) = torch.max(x4, dim=-1)
        x4 = torch.mean(bottleneck4, dim=-1)

        x = self.conv_block5(x)
        (bottleneck, _) = torch.max(x, dim=-1)
        '''(samples_num, feature_maps, time_steps)'''

        # Averaging out time axis
        x = torch.mean(bottleneck, dim=-1)

        '''
        x2 = self.conv_block1(x2)
        x2 = self.conv_block2(x2)
        x2 = self.conv_block3(x2)
        x2 = self.conv_block4(x2)
        (bottleneck2, _) = torch.max(x2, dim=-1)

        # Averaging out time axis
        x2 = torch.mean(bottleneck2, dim=-1)
        #print(x.shape, x2.shape)
        x = torch.cat((x, x2), 1)
        '''
        y = self.fc_final(x)
        y1 = self.fc_final_1(x1)
        y2 = self.fc_final_2(x2)
        y3 = self.fc_final_3(x3)
        y4 = self.fc_final_6(x4)

        out = torch.cat((y, y1, y2, y3, y4), dim=1)
        # out = F.relu_(self.fc_final_4(out))
        out = F.relu_(self.fc_final_4(out))
        out = self.fc_final_5(out)

        return out, y, y1, y2, y3, y4


class Vggish_FSDKaggle2019(nn.Module):
    def __init__(self, in_channels, classes_num):
        super(Vggish_FSDKaggle2019, self).__init__()

        self.conv_block1 = VggishConvBlock(in_channels=in_channels, out_channels=64)
        self.conv_block2 = VggishConvBlock(in_channels=64, out_channels=128)
        self.conv_block3 = VggishConvBlock(in_channels=128, out_channels=256)
        self.conv_block4 = VggishConvBlock(in_channels=256, out_channels=512)
        self.conv_block5 = VggishConvBlock(in_channels=512, out_channels=1024)

        self.fc_final_curated = nn.Linear(1024, classes_num, bias=True)
        self.fc_final_noisy = nn.Linear(1024, classes_num, bias=True)
        self.conv_drop = nn.Dropout2d(0.3)
        self.drop = nn.Dropout(0.4)
        self.init_weights()

    def init_weights(self):
        init_layer(self.fc_final_curated)
        init_layer(self.fc_final_noisy)

    def forward(self, input, return_bottleneck=False):
        # (_, seq_len, mel_bins) = input.shape

        # x = input.view(-1, 1, seq_len, mel_bins)
        #print(x.shape)
        x = input
        '''(samples_num, feature_maps, time_steps, freq_num)'''

        x = self.conv_block1(x)
        x = self.conv_block2(x)
        x = self.conv_block3(x)
        x = self.conv_block4(x)
        x = self.conv_block5(x)
        (bottleneck, _) = torch.max(x, dim=-1)
        '''(samples_num, feature_maps, time_steps)'''

        # Averaging out time axis
        x = torch.mean(bottleneck, dim=-1)
        '''(samples_num, feature_maps)'''

        y = self.fc_final_curated(x)
        # y = torch.sigmoid(self.fc_final(x))

        return y

    def noisy(self, input, return_bottleneck=False):
        # (_, seq_len, mel_bins) = input.shape

        # x = input.view(-1, 1, seq_len, mel_bins)
        #print(x.shape)
        x = input
        '''(samples_num, feature_maps, time_steps, freq_num)'''

        x = self.conv_block1(x)
        x = self.conv_block2(x)
        x = self.conv_block3(x)
        x = self.conv_block4(x)
        x = self.conv_block5(x)
        (bottleneck, _) = torch.max(x, dim=-1)
        '''(samples_num, feature_maps, time_steps)'''

        # Averaging out time axis
        x = torch.mean(bottleneck, dim=-1)
        '''(samples_num, feature_maps)'''

        y = self.fc_final_noisy(x)
        # y = torch.sigmoid(self.fc_final(x))

        return y


class Model_SpeechEmotion(nn.Module):
    def __init__(self, hidden_size=128):
        super(Model_SpeechEmotion, self).__init__()
        self.conv1 = nn.Sequential(
            nn.Conv2d(1, 64, kernel_size=(3, 3), padding=1),
            nn.BatchNorm2d(64),
            nn.ReLU(),
            nn.MaxPool2d(2, padding=1))
        self.conv2 = nn.Sequential(
            nn.Conv2d(64, 64, kernel_size=(3, 3), padding=1),
            nn.BatchNorm2d(64),
            nn.ReLU(),
            nn.MaxPool2d(2, padding=1))
        self.conv3 = nn.Sequential(
            nn.Conv2d(64, 64, kernel_size=(3, 3), padding=1),
            nn.BatchNorm2d(64),
            nn.ReLU(),
            nn.MaxPool2d(2, padding=1))
        self.conv4 = nn.Sequential(
            nn.Conv2d(64, 256, kernel_size=(3, 3), padding=1),
            nn.BatchNorm2d(256),
            nn.ReLU(),
            nn.MaxPool2d(2, padding=1))
        self.dropout = nn.Dropout(0.5)
        # LSTM(特征尺度，隐藏层），输入是[batch,时序，特征]，输出是[batch,len，hidden_size*2]
        self.hidden_size = hidden_size
        self.lstm = nn.LSTM(256, self.hidden_size, batch_first=True, bidirectional=True)
        self.dnn = nn.Linear(self.hidden_size * 2, 4)

    def forward(self, x, lengths):
        lengths = lengths.clone().detach()
        x = x.view(x.shape[0], 1, x.shape[1], x.shape[2])
        out = self.conv1(x)
        lengths = lengths // 2 + 1
        out = self.conv2(out)
        lengths = lengths // 2 + 1
        out = self.conv3(out)
        lengths = lengths // 2 + 1
        out = self.conv4(out)

        out = self.dropout(out)

        lengths = lengths // 2 + 1
        lengths = lengths

        out = torch.nn.functional.adaptive_avg_pool2d(out, (1, out.size()[3]))
        # 把通道和特征维度相乘，LSTM的输入是（batch，时序，特征），之后交换特征和时序位置
        out = out.view(out.size()[0], out.size()[1] * out.size()[2], -1)
        out = out.transpose(1, 2).contiguous()
        out = torch.nn.utils.rnn.pack_padded_sequence(out, lengths.cpu(), batch_first=True)

        out, (hn, cn) = self.lstm(out)

        out, _ = torch.nn.utils.rnn.pad_packed_sequence(out, batch_first=True)
        # average over lstm output, ignoring zero-padding part
        mask = (torch.arange(lengths[0]).cuda()[None, :] < lengths[:, None]).float()
        res = out * mask.unsqueeze(-1)  # res: (batch, lengths[0], hidden_size)
        res = res.sum(dim=1)  # res:(batch, hidden_size)
        res = res / (lengths.unsqueeze(-1))  # res:(batch, hidden_size)
        out = self.dnn(res)
        return out, 0
