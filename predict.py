import base64

import librosa
import scipy

from model import *
import torch
torch.backends.cudnn.deterministic = True
torch.backends.cudnn.benchmark = False
from torch.utils import data
import pandas as pd
from torch.autograd import Variable
from config import *
import numpy as np
import matplotlib.pyplot as plt
import shutil


def base64_somefile(path):
    with open(path, 'rb') as f:
        base64_data = base64.b64encode(f.read())

    return base64_data


class Predict(object):
    def __init__(self, esc50_model=None, urbansound_model=None, FSDKaggle2019_model=None, SpeechEmotion_model=None,
                 speech_emotion_only=True):
        super(Predict, self).__init__()
        self.speech_emotion_only=speech_emotion_only
        if not self.speech_emotion_only:
            self.esc50_zhuceng_model = esc50_model
            self.esc50_zhouqi_model = esc50_model
            self.esc50_chixu_model = esc50_model
            self.esc50_xiezhen_model = esc50_model

            self.urbansound_zhuceng_model1 = urbansound_model
            self.urbansound_zhuceng_model2 = urbansound_model
            self.urbansound_zhuceng_model3 = urbansound_model
            self.urbansound_zhuceng_model4 = urbansound_model
            self.urbansound_zhuceng_model5 = urbansound_model
            self.urbansound_zhuceng_model6 = urbansound_model
            self.urbansound_zhuceng_model7 = urbansound_model
            self.urbansound_zhuceng_model8 = urbansound_model
            self.urbansound_zhuceng_model9 = urbansound_model
            self.urbansound_zhuceng_model10 = urbansound_model
            self.urbansound_zhouqi_model1 = urbansound_model
            self.urbansound_zhouqi_model2 = urbansound_model
            self.urbansound_zhouqi_model3 = urbansound_model
            self.urbansound_zhouqi_model4 = urbansound_model
            self.urbansound_zhouqi_model5 = urbansound_model
            self.urbansound_zhouqi_model6 = urbansound_model
            self.urbansound_zhouqi_model7 = urbansound_model
            self.urbansound_zhouqi_model8 = urbansound_model
            self.urbansound_zhouqi_model9 = urbansound_model
            self.urbansound_zhouqi_model10 = urbansound_model
            self.urbansound_chixu_model1 = urbansound_model
            self.urbansound_chixu_model2 = urbansound_model
            self.urbansound_chixu_model3 = urbansound_model
            self.urbansound_chixu_model4 = urbansound_model
            self.urbansound_chixu_model5 = urbansound_model
            self.urbansound_chixu_model6 = urbansound_model
            self.urbansound_chixu_model7 = urbansound_model
            self.urbansound_chixu_model8 = urbansound_model
            self.urbansound_chixu_model9 = urbansound_model
            self.urbansound_chixu_model10 = urbansound_model
            self.urbansound_xiezhen_model1 = urbansound_model
            self.urbansound_xiezhen_model2 = urbansound_model
            self.urbansound_xiezhen_model3 = urbansound_model
            self.urbansound_xiezhen_model4 = urbansound_model
            self.urbansound_xiezhen_model5 = urbansound_model
            self.urbansound_xiezhen_model6 = urbansound_model
            self.urbansound_xiezhen_model7 = urbansound_model
            self.urbansound_xiezhen_model8 = urbansound_model
            self.urbansound_xiezhen_model9 = urbansound_model
            self.urbansound_xiezhen_model10 = urbansound_model
            self.urban_model_list = [self.urbansound_zhuceng_model1,
                                     self.urbansound_zhuceng_model2,
                                     self.urbansound_zhuceng_model3,
                                     self.urbansound_zhuceng_model4,
                                     self.urbansound_zhuceng_model5,
                                     self.urbansound_zhuceng_model6,
                                     self.urbansound_zhuceng_model7,
                                     self.urbansound_zhuceng_model8,
                                     self.urbansound_zhuceng_model9,
                                     self.urbansound_zhuceng_model10,
                                     self.urbansound_zhouqi_model1,
                                     self.urbansound_zhouqi_model2,
                                     self.urbansound_zhouqi_model3,
                                     self.urbansound_zhouqi_model4,
                                     self.urbansound_zhouqi_model5,
                                     self.urbansound_zhouqi_model6,
                                     self.urbansound_zhouqi_model7,
                                     self.urbansound_zhouqi_model8,
                                     self.urbansound_zhouqi_model9,
                                     self.urbansound_zhouqi_model10,
                                     self.urbansound_chixu_model1,
                                     self.urbansound_chixu_model2,
                                     self.urbansound_chixu_model3,
                                     self.urbansound_chixu_model4,
                                     self.urbansound_chixu_model5,
                                     self.urbansound_chixu_model6,
                                     self.urbansound_chixu_model7,
                                     self.urbansound_chixu_model8,
                                     self.urbansound_chixu_model9,
                                     self.urbansound_chixu_model10,
                                     self.urbansound_xiezhen_model1,
                                     self.urbansound_xiezhen_model2,
                                     self.urbansound_xiezhen_model3,
                                     self.urbansound_xiezhen_model4,
                                     self.urbansound_xiezhen_model5,
                                     self.urbansound_xiezhen_model6,
                                     self.urbansound_xiezhen_model7,
                                     self.urbansound_xiezhen_model8,
                                     self.urbansound_xiezhen_model9,
                                     self.urbansound_xiezhen_model10
                                     ]
            self.FSDKaggle2019_model = FSDKaggle2019_model

        self.SpeechEmotion_model = SpeechEmotion_model

        self.init_model_state()

    def init_model_state(self):
        print("init model")
        if not self.speech_emotion_only:
            self.esc50_zhuceng_model.load_state_dict(torch.load(model_path["esc50_path_zhuceng"]))
            self.esc50_zhouqi_model.load_state_dict(torch.load(model_path["esc50_path_zhouqi"]))
            self.esc50_chixu_model.load_state_dict(torch.load(model_path["esc50_path_chixu"]))
            self.esc50_xiezhen_model.load_state_dict(torch.load(model_path["esc50_path_xiezhen"]))
            i = 0
            for key in model_path:
                print(model_path[key])
                self.urban_model_list[i].load_state_dict(torch.load(model_path[key]))
                i += 1
                if i == 39:
                    break
            self.FSDKaggle2019_model.load_state_dict(torch.load(model_path["FSDKaggle_path"]))
        self.SpeechEmotion_model.load_state_dict(torch.load(model_path["SpeechEmotion_path"]))
        print("finish loading model")

    def predict_esc_labels(self, inputs):
        self.esc50_zhuceng_model.eval()
        self.esc50_zhouqi_model.eval()
        self.esc50_chixu_model.eval()
        self.esc50_xiezhen_model.eval()

        inputs = inputs / 255
        inputs = inputs.swapaxes(0, 2)
        inputs = torch.Tensor(inputs.swapaxes(1, 2))
        inputs = Variable(inputs).cuda()
        inputs = inputs.unsqueeze(0)

        outputs_zhuceng, _, _, _, _, _ = self.esc50_zhuceng_model(inputs)
        outputs_zhouqi, _, _, _, _, _ = self.esc50_zhouqi_model(inputs)
        outputs_chixu, _, _, _, _, _ = self.esc50_chixu_model(inputs)
        outputs_xiezhen, _, _, _, _, _ = self.esc50_xiezhen_model(inputs)
        outputs_zhuceng.detach_()
        outputs_zhouqi.detach_()
        outputs_chixu.detach_()
        outputs_xiezhen.detach_()
        _, predicted_zhuceng = torch.max(outputs_zhuceng.data, 1)
        _, predicted_zhouqi = torch.max(outputs_zhouqi.data, 1)
        _, predicted_chixu = torch.max(outputs_chixu.data, 1)
        _, predicted_xiezhen = torch.max(outputs_xiezhen.data, 1)
        predicted_zhuceng = int(predicted_zhuceng.cpu().numpy())
        predicted_zhouqi = int(predicted_zhouqi.cpu().numpy())
        predicted_chixu = int(predicted_chixu.cpu().numpy())
        predicted_xiezhen = int(predicted_xiezhen.cpu().numpy())
        return esc50_labels[predicted_zhuceng], esc50_labels[predicted_zhouqi], \
               esc50_labels[predicted_chixu], esc50_labels[predicted_xiezhen]

    def predict_urbansound_labels(self, inputs):
        self.urbansound_zhuceng_model1.eval()
        self.urbansound_zhuceng_model2.eval()
        self.urbansound_zhuceng_model3.eval()
        self.urbansound_zhuceng_model4.eval()
        self.urbansound_zhuceng_model5.eval()
        self.urbansound_zhuceng_model6.eval()
        self.urbansound_zhuceng_model7.eval()
        self.urbansound_zhuceng_model8.eval()
        self.urbansound_zhuceng_model9.eval()
        self.urbansound_zhuceng_model10.eval()
        self.urbansound_zhouqi_model1.eval()
        self.urbansound_zhouqi_model2.eval()
        self.urbansound_zhouqi_model3.eval()
        self.urbansound_zhouqi_model4.eval()
        self.urbansound_zhouqi_model5.eval()
        self.urbansound_zhouqi_model6.eval()
        self.urbansound_zhouqi_model7.eval()
        self.urbansound_zhouqi_model8.eval()
        self.urbansound_zhouqi_model9.eval()
        self.urbansound_zhouqi_model10.eval()
        self.urbansound_chixu_model1.eval()
        self.urbansound_chixu_model2.eval()
        self.urbansound_chixu_model3.eval()
        self.urbansound_chixu_model4.eval()
        self.urbansound_chixu_model5.eval()
        self.urbansound_chixu_model6.eval()
        self.urbansound_chixu_model7.eval()
        self.urbansound_chixu_model8.eval()
        self.urbansound_chixu_model9.eval()
        self.urbansound_chixu_model10.eval()
        self.urbansound_xiezhen_model1.eval()
        self.urbansound_xiezhen_model2.eval()
        self.urbansound_xiezhen_model3.eval()
        self.urbansound_xiezhen_model4.eval()
        self.urbansound_xiezhen_model5.eval()
        self.urbansound_xiezhen_model6.eval()
        self.urbansound_xiezhen_model7.eval()
        self.urbansound_xiezhen_model8.eval()
        self.urbansound_xiezhen_model9.eval()
        self.urbansound_xiezhen_model10.eval()

        inputs = inputs / 255
        inputs = inputs.swapaxes(0, 2)
        inputs = torch.Tensor(inputs.swapaxes(1, 2))
        inputs = Variable(inputs).cuda()
        inputs = inputs.unsqueeze(0)

        outputs_zhuceng1, _, _, _, _, _ = self.urbansound_zhuceng_model1(inputs)
        outputs_zhuceng2, _, _, _, _, _ = self.urbansound_zhuceng_model2(inputs)
        outputs_zhuceng3, _, _, _, _, _ = self.urbansound_zhuceng_model3(inputs)
        outputs_zhuceng4, _, _, _, _, _ = self.urbansound_zhuceng_model4(inputs)
        outputs_zhuceng5, _, _, _, _, _ = self.urbansound_zhuceng_model5(inputs)
        outputs_zhuceng6, _, _, _, _, _ = self.urbansound_zhuceng_model6(inputs)
        outputs_zhuceng7, _, _, _, _, _ = self.urbansound_zhuceng_model7(inputs)
        outputs_zhuceng8, _, _, _, _, _ = self.urbansound_zhuceng_model8(inputs)
        outputs_zhuceng9, _, _, _, _, _ = self.urbansound_zhuceng_model9(inputs)
        outputs_zhuceng10, _, _, _, _, _ = self.urbansound_zhuceng_model10(inputs)
        outputs_zhuceng = (
                                      outputs_zhuceng1 + outputs_zhuceng2 + outputs_zhuceng3 + outputs_zhuceng4 + outputs_zhuceng5 +
                                      outputs_zhuceng6 + outputs_zhuceng7 + outputs_zhuceng8 + outputs_zhuceng9 + outputs_zhuceng10) * 0.1
        outputs_zhouqi1, _, _, _, _, _ = self.urbansound_zhouqi_model1(inputs)
        outputs_zhouqi2, _, _, _, _, _ = self.urbansound_zhouqi_model2(inputs)
        outputs_zhouqi3, _, _, _, _, _ = self.urbansound_zhouqi_model3(inputs)
        outputs_zhouqi4, _, _, _, _, _ = self.urbansound_zhouqi_model4(inputs)
        outputs_zhouqi5, _, _, _, _, _ = self.urbansound_zhouqi_model5(inputs)
        outputs_zhouqi6, _, _, _, _, _ = self.urbansound_zhouqi_model6(inputs)
        outputs_zhouqi7, _, _, _, _, _ = self.urbansound_zhouqi_model7(inputs)
        outputs_zhouqi8, _, _, _, _, _ = self.urbansound_zhouqi_model8(inputs)
        outputs_zhouqi9, _, _, _, _, _ = self.urbansound_zhouqi_model9(inputs)
        outputs_zhouqi10, _, _, _, _, _ = self.urbansound_zhouqi_model10(inputs)
        outputs_zhouqi = (outputs_zhouqi1 + outputs_zhouqi2 + outputs_zhouqi3 + outputs_zhouqi4 + outputs_zhouqi5 +
                          outputs_zhouqi6 + outputs_zhouqi7 + outputs_zhouqi8 + outputs_zhouqi9 + outputs_zhouqi10) * 0.1

        outputs_chixu1, _, _, _, _, _ = self.urbansound_chixu_model1(inputs)
        outputs_chixu2, _, _, _, _, _ = self.urbansound_chixu_model2(inputs)
        outputs_chixu3, _, _, _, _, _ = self.urbansound_chixu_model3(inputs)
        outputs_chixu4, _, _, _, _, _ = self.urbansound_chixu_model4(inputs)
        outputs_chixu5, _, _, _, _, _ = self.urbansound_chixu_model5(inputs)
        outputs_chixu6, _, _, _, _, _ = self.urbansound_chixu_model6(inputs)
        outputs_chixu7, _, _, _, _, _ = self.urbansound_chixu_model7(inputs)
        outputs_chixu8, _, _, _, _, _ = self.urbansound_chixu_model8(inputs)
        outputs_chixu9, _, _, _, _, _ = self.urbansound_chixu_model9(inputs)
        outputs_chixu10, _, _, _, _, _ = self.urbansound_chixu_model10(inputs)
        outputs_chixu = (outputs_chixu1 + outputs_chixu2 + outputs_chixu3 + outputs_chixu4 + outputs_chixu5 +
                         outputs_chixu6 + outputs_chixu7 + outputs_chixu8 + outputs_chixu9 + outputs_chixu10) * 0.1

        outputs_xiezhen1, _, _, _, _, _ = self.urbansound_xiezhen_model1(inputs)
        outputs_xiezhen2, _, _, _, _, _ = self.urbansound_xiezhen_model2(inputs)
        outputs_xiezhen3, _, _, _, _, _ = self.urbansound_xiezhen_model3(inputs)
        outputs_xiezhen4, _, _, _, _, _ = self.urbansound_xiezhen_model4(inputs)
        outputs_xiezhen5, _, _, _, _, _ = self.urbansound_xiezhen_model5(inputs)
        outputs_xiezhen6, _, _, _, _, _ = self.urbansound_xiezhen_model6(inputs)
        outputs_xiezhen7, _, _, _, _, _ = self.urbansound_xiezhen_model7(inputs)
        outputs_xiezhen8, _, _, _, _, _ = self.urbansound_xiezhen_model8(inputs)
        outputs_xiezhen9, _, _, _, _, _ = self.urbansound_xiezhen_model9(inputs)
        outputs_xiezhen10, _, _, _, _, _ = self.urbansound_xiezhen_model10(inputs)
        outputs_xiezhen = (
                                      outputs_xiezhen1 + outputs_xiezhen2 + outputs_xiezhen3 + outputs_xiezhen4 + outputs_xiezhen5 +
                                      outputs_xiezhen6 + outputs_xiezhen7 + outputs_xiezhen8 + outputs_xiezhen9 + outputs_xiezhen10) * 0.1
        outputs_zhuceng.detach_()
        outputs_zhouqi.detach_()
        outputs_chixu.detach_()
        outputs_xiezhen.detach_()
        _, predicted_zhuceng = torch.max(outputs_zhuceng.data, 1)
        _, predicted_zhouqi = torch.max(outputs_zhouqi.data, 1)
        _, predicted_chixu = torch.max(outputs_chixu.data, 1)
        _, predicted_xiezhen = torch.max(outputs_xiezhen.data, 1)
        predicted_zhuceng = int(predicted_zhuceng.cpu().numpy())
        predicted_zhouqi = int(predicted_zhouqi.cpu().numpy())
        predicted_chixu = int(predicted_chixu.cpu().numpy())
        predicted_xiezhen = int(predicted_xiezhen.cpu().numpy())
        return urbansound_labels[predicted_zhuceng], urbansound_labels[predicted_zhouqi], \
               urbansound_labels[predicted_chixu], urbansound_labels[predicted_xiezhen]

    def predict_kaggleFSD2019_labels(self, inputs):
        num_classes = 80
        sigmoid = torch.nn.Sigmoid().cuda()
        inputs = torch.Tensor(inputs)
        inputs = inputs.unsqueeze(0)
        inputs = torch.autograd.Variable(inputs.cuda())
        output = self.FSDKaggle2019_model(inputs)
        pred = sigmoid(output[0])
        pred = pred.data.cpu().numpy()
        tmp = pred / np.max(pred)
        return_labels = np.where(tmp > 0.5)
        retrieved_classes = []
        for i in return_labels[0]:
            retrieved_classes.append(FSDKaggle_labels[i])
        # retrieved_classes = np.argsort(pred)[::-1]
        # print(retrieved_classes)
        # return list(retrieved_classes.astype(float))
        return retrieved_classes

    def get_emotion_scores(self, inputs):
        inp = torch.from_numpy(inputs).unsqueeze(0).float().cuda()  # (1, freq, time)
        lengths = torch.tensor([inp.shape[-1]]).cuda()
        out, _ = self.SpeechEmotion_model(inp, lengths)
        scores = torch.softmax(out, dim=-1)
        scores = scores.tolist()[0]
        # print('emotion predicted as : {emotion}'.format(emotion=self.dic[res]))
        return scores

    def prepare_plot_and_wav(self, path):
        y, sr = librosa.load(path, sr=None)
        y = scipy.signal.lfilter(([1, -0.97]), 1, y)
        melspec = librosa.feature.melspectrogram(y, sr, n_fft=512, hop_length=256, n_mels=128)
        logmelspec = librosa.power_to_db(melspec)
        a = plt.figure()
        plt.subplot(211)
        librosa.display.specshow(logmelspec, y_axis='mel', sr=sr, hop_length=256,
                                 x_axis='time')
        plt.subplot(212)
        librosa.display.waveplot(y, sr)
        tmp_path = './tmp.jpg'
        plt.savefig(tmp_path)
        plt.close()
        img_base64 = base64_somefile(tmp_path)
        wav_base64 = base64_somefile(path)
        return img_base64, wav_base64

# predict_object = Predict(Vggish_esc(3, 50).cuda(), Vggish_urbansound(3, 10).cuda(), Vggish_FSDKaggle2019(1, 80).cuda(),
#                          Model_SpeechEmotion().cuda())
predict_object = Predict(SpeechEmotion_model=Model_SpeechEmotion().cuda(), speech_emotion_only=True)
