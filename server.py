# coding: utf-8
import tornado.ioloop
import tornado.web
import shutil
import os
import json
from handlers import Base, ESC50, SpeechEmotion, urbansound8k, FSDKaggle2019, SpeechEmotion


app = tornado.web.Application([
    (r'/', Base.BaseHandler),
    (r'/esc50', ESC50.ESC50Handler),
    (r'/urbansound8k', urbansound8k.Urbansound8kHandler),
    (r'/FSDKaggle2019', FSDKaggle2019.FSDKaggle2019Handler),
    (r'/speech_emotion', SpeechEmotion.SpeechEmotionHandler),
], static_path=os.path.join(os.path.dirname(__file__), 'static')
)

if __name__ == '__main__':
    app.listen(8882)
    tornado.ioloop.IOLoop.instance().start()